//
//  VTVector.cpp
//  MovementDetection
//
//  Created by Viet Trinh on 4/9/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#include "VTVector.h"
#include <cmath>

VTVector::VTVector(){
    this->x = this->y = this->z = 0.0;
}

VTVector::VTVector(float xp,float yp,float zp){
    setXComponent(xp);
    setYComponent(yp);
    setZComponent(zp);
}

float VTVector::XComponent(){
    return this->x;
}

float VTVector::YComponent(){
    return this->y;
}

float VTVector::ZComponent(){
    return this->z;
}

void VTVector::setXComponent(float xp){
    this->x = xp;
}

void VTVector::setYComponent(float yp){
    this->y = yp;
}

void VTVector::setZComponent(float zp){
    this->z = zp;
}

void VTVector::setComponents(float xp,float yp,float zp){
    setXComponent(xp);
    setYComponent(yp);
    setZComponent(zp);
}

void VTVector::setComponents(VTVector v){
    setXComponent(v.XComponent());
    setYComponent(v.YComponent());
    setZComponent(v.ZComponent());
}

float VTVector::length(){
    return sqrtf(pow(XComponent(), 2.0) +
                 pow(YComponent(), 2.0) +
                 pow(ZComponent(), 2.0));
}

void VTVector::normalize(){
    float length = this->length();
    setXComponent(XComponent()/length);
    setYComponent(YComponent()/length);
    setZComponent(ZComponent()/length);
}

void VTVector::multiplyByScalar(float k){
    this->x *= k;
    this->y *= k;
    this->z *= k;
}

/**
 *
 * Right hand rule: Thumb is the direction of normal vector,
 * curve of fingers is the orientation of vertices
 * n = ab x bc
 *
 **/
VTVector VTVector::normalVectorOfPlaneContaining(VTPoint a, VTPoint b, VTPoint c){

    VTVector normal,ab,bc;
    
    // Vector ab
    ab.setXComponent(b.XCoord() - a.XCoord());
    ab.setYComponent(b.YCoord() - a.YCoord());
    ab.setZComponent(b.ZCoord() - a.ZCoord());
    
    // Vector bc
    bc.setXComponent(c.XCoord() - b.XCoord());
    bc.setYComponent(c.YCoord() - b.YCoord());
    bc.setZComponent(c.ZCoord() - b.ZCoord());
    
    // Vector ab x bc
    normal.setXComponent(ab.YComponent()*bc.ZComponent() - bc.YComponent()*ab.ZComponent());
    normal.setYComponent(ab.ZComponent()*bc.XComponent() - bc.ZComponent()*ab.XComponent());
    normal.setZComponent(ab.XComponent()*bc.YComponent() - bc.XComponent()*ab.YComponent());
    normal.normalize();
    
    return normal;
}

VTVector VTVector::addVectors(VTVector a, VTVector b){
    VTVector resultant;
    resultant.setComponents(a.XComponent() + b.XComponent(),
                            a.YComponent() + b.YComponent(),
                            a.ZComponent() + b.ZComponent());
    return resultant;
}

VTVector VTVector::subtractVectors(VTVector a, VTVector b){
    VTVector resultant;
    resultant.setComponents(a.XComponent() - b.XComponent(),
                            a.YComponent() - b.YComponent(),
                            a.ZComponent() - b.ZComponent());
    return resultant;
}

VTVector VTVector::crossProdtuct(VTVector a, VTVector b){
    VTVector resultant;
    resultant.setComponents(a.YComponent()*b.ZComponent()-a.ZComponent()*b.YComponent(),
                            a.ZComponent()*b.XComponent()-a.XComponent()*b.ZComponent(),
                            a.XComponent()*b.YComponent()-a.YComponent()*b.XComponent());
    return resultant;
}

float VTVector::dotProduct(VTVector a, VTVector b){
    return a.XComponent()*b.XComponent() + a.YComponent()*b.YComponent() + a.ZComponent()*b.ZComponent();
}

std::ostream& operator << (std::ostream& os, VTVector& vector){
    return os << "(" << vector.XComponent() << ", " << vector.YComponent() << ", " << vector.ZComponent() << ")";
}


