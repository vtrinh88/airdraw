//
//  VTVector.h
//  MovementDetection
//
//  Created by Viet Trinh on 4/9/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifndef __MovementDetection__VTVector__
#define __MovementDetection__VTVector__

#include <stdio.h>
#include <iostream>
#include "VTPoint.h"

class VTVector{
    float x;
    float y;
    float z;
 public:
    VTVector();
    VTVector(float xp,float yp,float zp);
    float XComponent();
    float YComponent();
    float ZComponent();
    void setXComponent(float xp);
    void setYComponent(float yp);
    void setZComponent(float zp);
    void setComponents(float xp,float yp,float zp);
    void setComponents(VTVector v);
    float length();
    void normalize();
    void multiplyByScalar(float k);
    static VTVector normalVectorOfPlaneContaining(VTPoint a, VTPoint b, VTPoint c);
    static VTVector addVectors(VTVector a, VTVector b);
    static VTVector subtractVectors(VTVector a, VTVector b);
    static VTVector crossProdtuct(VTVector a, VTVector b);
    static float dotProduct(VTVector a, VTVector b);
    friend std::ostream& operator << (std::ostream& os, VTVector& vector);
};

#endif /* defined(__MovementDetection__Vector__) */
