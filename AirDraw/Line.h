//
//  Line.h
//  AirDraw
//
//  Created by Viet Trinh on 4/20/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifndef __AirDraw__Line__
#define __AirDraw__Line__

#include <stdio.h>
#include "VTPoint.h"
#include "VTVector.h"
#include "Constants.h"

class Line{
    VTPoint* points;
    VTVector color;
    int numPtsOnLine;
    void randomizeLineColor();
  public:
    Line();
    int getNumberOfPointsOnLine();
    void addPointToLine(VTPoint point);
    void deleteAllPointsOnLine();
    VTPoint pointAtIndex(int index);
    VTVector getLineColor();
    void setLineColor(float r, float g, float b);
};

#endif /* defined(__AirDraw__Line__) */
