//
//  Line.cpp
//  AirDraw
//
//  Created by Viet Trinh on 4/20/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#include "Line.h"
#include <cstdlib>
#include <iostream>

Line::Line(){
    this->points = new VTPoint[MAX_POINTS_ON_LINE];
    this->numPtsOnLine = 0;
    this->randomizeLineColor();
}

int Line::getNumberOfPointsOnLine(){
    return this->numPtsOnLine;
}

void Line::addPointToLine(VTPoint point){
    this->points[numPtsOnLine] = point;
    numPtsOnLine++;
}

void Line::deleteAllPointsOnLine(){
    delete [] this->points;
    this->points = new VTPoint[MAX_POINTS_ON_LINE];
    this->numPtsOnLine = 0;
    this->randomizeLineColor();
}

VTPoint Line::pointAtIndex(int index){
    return (index < this->numPtsOnLine) ? this->points[index] : VTPoint(0,0,0);
}

void Line::randomizeLineColor(){
    float red   = (float)rand() / RAND_MAX;
    float green = (float)rand() / RAND_MAX;
    float blue  = (float)rand() / RAND_MAX;
    this->color.setComponents(red, green, blue);
}

VTVector Line::getLineColor(){
    return this->color;
}

void Line::setLineColor(float r, float g, float b){
    this->color.setComponents(r, g, b);
}