//
//  VTPoint.cpp
//  MovementDetection
//
//  Created by Viet Trinh on 4/9/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#include "VTPoint.h"

VTPoint::VTPoint(){
    this->x = this->y = this->z = 0.0;
}

VTPoint::VTPoint(float xp,float yp,float zp){
    setXCoord(xp);
    setYCoord(yp);
    setZCoord(zp);
}

float VTPoint::XCoord(){
    return this->x;
}

float VTPoint::YCoord(){
    return this->y;
}

float VTPoint::ZCoord(){
    return this->z;
}

void VTPoint::setXCoord(float xp){
    this->x = xp;
}

void VTPoint::setYCoord(float yp){
    this->y = yp;
}

void VTPoint::setZCoord(float zp){
    this->z = zp;
}

void VTPoint::setCoordinates(float xp,float yp,float zp){
    setXCoord(xp);
    setYCoord(yp);
    setZCoord(zp);
}

std::ostream& operator << (std::ostream& os, VTPoint& point){
    return os << "(" << point.XCoord() << ", " << point.YCoord() << ", " << point.ZCoord() << ")";
}