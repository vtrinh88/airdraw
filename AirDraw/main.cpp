//
//  main.cpp
//  AirDraw
//
//  Created by Viet Trinh on 4/16/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include "VTPoint.h"
#include "VTVector.h"
#include "Constants.h"
#include "Leap.h"
#include "MotionListener.h"
#include "Line.h"

using namespace std;
using namespace Leap;

/*----- GLOBAL VARIABLES ----------------------------*/
int    button, state;
float  gx, gy, XWIN, YWIN;
Controller controller;
MotionListener listener;
Line line;

/*----- FUNCTIONS DECLARATION -----------------------*/
void init();
void setupScreen();
void displayScreen();
void mouseClicks(int but,int sta,int x,int y);
void mouseMoves(int x, int y);
void keyPresses(unsigned char c, int x, int y);
void cleanUp();

void drawScene();
void drawLines();
VTVector convertFromLeapCoordinatesToDisplayCoordinates(Vector vector);

/*----- FUNCTIONS DEFINITION -----------------------*/
int main(int argc, char * argv[]) {
    init();
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    
    // Create a window
    glutCreateWindow("Air Drawing");
    glutPositionWindow(0, 0);
    glutReshapeWindow(XWIN, YWIN);
    
    // Program start here...
    glutDisplayFunc(displayScreen);
    glutMouseFunc(mouseClicks);
    glutMotionFunc(mouseMoves);
    glutKeyboardFunc(keyPresses);
    glutMainLoop();
    cleanUp();
    
    return 0;
}


void init(){
    button      = state = 1;
    XWIN        = 800.0;
    YWIN        = 600.0;
    controller.addListener(listener);
    line.setLineColor(1.0, 1.0, 1.0);
}


void cleanUp(){
    controller.removeListener(listener);
}


void setupScreen(){
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0,1.0,0.0,1.0);	// set coord for viewport
    glViewport(0,0,XWIN,YWIN);      // set viewport
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void displayScreen(){
    setupScreen();
    drawScene();
    glutSwapBuffers();
    glutPostRedisplay();
}


void mouseClicks(int but,int sta,int x,int y){
    button = but;
    state = sta;
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        gx = float(x)/XWIN;
        gy = float(YWIN-y)/YWIN;
        
        // code here...
    }
    glutPostRedisplay();
}


void mouseMoves(int x, int y){
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
        gx = float(x)/XWIN;
        gy = float(YWIN-y)/YWIN;
        
        // code here..
    }
    glutPostRedisplay();
}


void keyPresses(unsigned char c, int x, int y){
    
    //more code here...
    glutPostRedisplay();
}


void drawScene(){
    
    /* tracking the right index fingertip for drawing position */
    HandList hands = controller.frame().hands();
    for (HandList::const_iterator handIterator = hands.begin(); handIterator != hands.end(); handIterator++) {
        const Hand hand = *handIterator;
        if (hand.isRight()) {
            FingerList fingers= hand.fingers();
            for (FingerList::const_iterator fingerIterator = fingers.begin(); fingerIterator != fingers.end(); fingerIterator++) {
                const Finger finger = *fingerIterator;
                if (finger.type() == Finger::TYPE_INDEX) {
                    Vector   tipPosLeapCoord = finger.tipPosition();
                    VTVector tipPosDispCoord = convertFromLeapCoordinatesToDisplayCoordinates(tipPosLeapCoord);
                    if (line.getNumberOfPointsOnLine() < MAX_POINTS_ON_LINE) {
                        line.addPointToLine(VTPoint(tipPosDispCoord.XComponent(),tipPosDispCoord.YComponent(),0));
                    }
                    cout << "tipPosLeap:" << tipPosLeapCoord <<" - tipPosDisp:"<< tipPosDispCoord << endl;
                }
            }
        }
    }
    
    /* 
     * If the SWIPE gesture is recognized, then clear the canvas.
     * Otherwise, keep drawing fingertip's position
     */
    GestureList gestures = controller.frame().gestures();
    if (gestures.count() > 0) {
        for (int g = 0; g < gestures.count(); g++) {
            Gesture gesture = gestures[g];
            if (gesture.type() == Gesture::TYPE_SWIPE)
                line.deleteAllPointsOnLine();
            else
                drawLines();
        }
    }
    else drawLines();
}


void drawLines(){
    VTVector color = line.getLineColor();
    glLineWidth(3.5);
    for (int i = 0; i<line.getNumberOfPointsOnLine()-1;i++){
        VTPoint leftEndPoint = line.pointAtIndex(i);
        VTPoint rightEndPoint = line.pointAtIndex(i+1);
        glBegin(GL_LINES);
        glColor3f(color.XComponent(),color.YComponent(),color.ZComponent());
        glVertex3f(leftEndPoint.XCoord(),leftEndPoint.YCoord(),leftEndPoint.ZCoord());
        glVertex3f(rightEndPoint.XCoord(),rightEndPoint.YCoord(),rightEndPoint.ZCoord());
        glEnd();
    }
}


VTVector convertFromLeapCoordinatesToDisplayCoordinates(Vector leapCoordinates){
    VTVector displayCoordinates, leapCoordRange, displayCoordRange;
    
    leapCoordRange.setComponents(LEAP_COORD_MAX_X - LEAP_COORD_MIN_X,
                                 LEAP_COORD_MAX_Y - LEAP_COORD_MIN_Y,
                                 LEAP_COORD_MAX_Z - LEAP_COORD_MIN_Z);
    
    displayCoordRange.setComponents(DISPLAY_COORD_MAX_X - DISPLAY_COORD_MIN_X,
                                    DISPLAY_COORD_MAX_Y - DISPLAY_COORD_MIN_Y,
                                    DISPLAY_COORD_MAX_Z - DISPLAY_COORD_MIN_Z);
    
    displayCoordinates.setXComponent(DISPLAY_COORD_MIN_X + (leapCoordinates.x - LEAP_COORD_MIN_X)*(displayCoordRange.XComponent()/leapCoordRange.XComponent()));
    displayCoordinates.setYComponent(DISPLAY_COORD_MIN_Y + (leapCoordinates.y - LEAP_COORD_MIN_Y)*(displayCoordRange.YComponent()/leapCoordRange.YComponent()));
    displayCoordinates.setZComponent(DISPLAY_COORD_MIN_Z + (leapCoordinates.z - LEAP_COORD_MIN_Z)*(displayCoordRange.ZComponent()/leapCoordRange.ZComponent()));
    
    return displayCoordinates;
}


