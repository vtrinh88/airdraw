//
//  VTPoint.h
//  MovementDetection
//
//  Created by Viet Trinh on 4/9/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifndef __MovementDetection__VTPoint__
#define __MovementDetection__VTPoint__

#include <stdio.h>
#include <iostream>

class VTPoint{
    float x;
    float y;
    float z;
 public:
    VTPoint();
    VTPoint(float xp,float yp,float zp);
    float XCoord();
    float YCoord();
    float ZCoord();
    void setXCoord(float xp);
    void setYCoord(float yp);
    void setZCoord(float zp);
    void setCoordinates(float xp,float yp,float zp);
    friend std::ostream& operator << (std::ostream& os, VTPoint& point);
};

#endif /* defined(__MovementDetection__VTPoint__) */
