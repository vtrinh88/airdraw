//
//  Constants.h
//  MovementDetection
//
//  Created by Viet Trinh on 4/15/15.
//  Copyright (c) 2015 VietTrinh. All rights reserved.
//

#ifndef MovementDetection_Constants_h
#define MovementDetection_Constants_h

/* LEAP COORDINATES RANGE */
#define LEAP_COORD_MIN_X -250
#define LEAP_COORD_MAX_X 250
#define LEAP_COORD_MIN_Y 0
#define LEAP_COORD_MAX_Y 250
#define LEAP_COORD_MIN_Z -150
#define LEAP_COORD_MAX_Z 150

/* DISPLAY COORDINATES RANGE */
#define DISPLAY_COORD_MIN_X 0
#define DISPLAY_COORD_MAX_X 1
#define DISPLAY_COORD_MIN_Y 0
#define DISPLAY_COORD_MAX_Y 1
#define DISPLAY_COORD_MIN_Z 0
#define DISPLAY_COORD_MAX_Z 0

#define MAX_POINTS_ON_LINE 1000
#endif
