# AirDraw #

This project employs [Leap Motion SDK](https://www.leapmotion.com/) for finger tracking, and displays a 3D contour path of a finger movement. [Video link](https://www.youtube.com/watch?v=ByP2r9oqORw)

### System Requirements ###

* [Leap sensor](https://www.leapmotion.com/)
* OpenGL

### How do I get set up? ###

Please contact me

### Copyright ###

Viet Trinh © 2015  
vqtrinh@ucsc.edu